package com.ahsanislam.kawansogrocery.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class GroceryListItem(
    var groceryItems:ArrayList<GroceryItem>?=ArrayList(),
    var name:String?="",
    var doneStatus:Boolean?=false,
    var timestamp: Long?=System.currentTimeMillis(), //timemillisec
) {
    @PrimaryKey(autoGenerate = true)
    var id:Int = 0
}