package com.ahsanislam.kawansogrocery.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.sql.Timestamp

data class GroceryItem(
    var id:Int,
    var name:String,
    var ammount:Double,
    var doneStatus:Boolean
)