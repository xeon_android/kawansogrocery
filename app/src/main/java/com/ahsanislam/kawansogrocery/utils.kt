package com.ahsanislam.kawansogrocery

import android.app.Activity
import android.view.View
import android.widget.PopupMenu

fun ArrayList<String>.showOptions(
    activity: Activity,
    view: View? = null,
    Callback: ((Int, String) -> Unit)? = null,
) {

    //creating a popup menu
    val popup = PopupMenu(activity, view)
    for (str in this) {
        popup.menu.add(str)  // menus items

    }
    //adding click listener
    popup.setOnMenuItemClickListener { item ->

        Callback?.invoke(this.indexOf(item.title.toString()), item.title.toString())
        true
    }
    popup.show()
}