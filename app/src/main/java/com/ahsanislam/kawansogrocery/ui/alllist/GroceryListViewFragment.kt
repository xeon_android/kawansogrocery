package com.ahsanislam.kawansogrocery.ui.alllist

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.ahsanislam.kawansogrocery.MainScreen
import com.ahsanislam.kawansogrocery.R
import com.ahsanislam.kawansogrocery.RoomDB.LocalDB
import com.ahsanislam.kawansogrocery.adapters.GroceryItemAdapter
import com.ahsanislam.kawansogrocery.databinding.FragmentGroceryListViewBinding


class GroceryListViewFragment : Fragment() {
    private var viewOfLayout: FragmentGroceryListViewBinding? = null
    lateinit var groceryItemAdapter: GroceryItemAdapter
    var selectedListId: Int = 0


    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = viewOfLayout!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewOfLayout = FragmentGroceryListViewBinding.inflate(inflater, container, false)
        val root: View = binding.root

        arguments?.let {
            selectedListId = it.getInt("selectedListId")
            initRecycler()
        }
        (requireActivity() as MainScreen).binding.btnAdd.setOnClickListener {
            (requireActivity() as MainScreen).navController.navigate(R.id.navigation_create_list)
        }

        return root
    }

    fun initRecycler() {
        groceryItemAdapter = GroceryItemAdapter { item, position ->

            item.doneStatus = true
            val list = LocalDB.getDatabase(requireContext())?.groceryDAO()?.get(selectedListId)
            list?.groceryItems?.find { x -> x.name == item.name }?.doneStatus = true
            LocalDB.getDatabase(requireContext())?.groceryDAO()?.update(list!!)
            groceryItemAdapter.update(item, position)


        }
        LocalDB.getDatabase(requireContext())?.groceryDAO()?.get(selectedListId)?.let {
            groceryItemAdapter.addAll(ArrayList(it.groceryItems))
        }

        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        viewOfLayout?.rvGroceryItemList1?.let {
            itemTouchHelper.attachToRecyclerView(it)
            it.adapter = groceryItemAdapter
        }

    }

    var simpleItemTouchCallback: ItemTouchHelper.SimpleCallback = object :
        ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT or ItemTouchHelper.DOWN or ItemTouchHelper.UP
        ) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
            //Remove swiped item from list and notify the RecyclerView
            val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
                .setMessage("Are you sure you want to delete this item?")
                .setPositiveButton("Yes, Delete") { dialogInterface, i ->
                    val position = viewHolder.adapterPosition
                    groceryItemAdapter.get(position)?.let {
                        val list =
                            LocalDB.getDatabase(requireContext())?.groceryDAO()?.get(selectedListId)
                        list?.groceryItems?.remove(list.groceryItems?.find { x -> x.name == it.name }!!)
                        if (list?.groceryItems.isNullOrEmpty()) {
                            //user deleted the last item means remove the list too
                            LocalDB.getDatabase(requireContext())?.groceryDAO()?.delete(list!!.id)
                            (requireActivity() as MainScreen).onBackPressed()
                        } else {
                            LocalDB.getDatabase(requireContext())?.groceryDAO()?.update(list!!)
                        }
                        groceryItemAdapter.delete(position)
                    }

                    // Dismiss Dialog
                    dialogInterface.dismiss()
                }
                .setNegativeButton("Cancel") { dialogInterface, i -> dialogInterface.cancel() }
            builder.show()

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewOfLayout = null
    }

}