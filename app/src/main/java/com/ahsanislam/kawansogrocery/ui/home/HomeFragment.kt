package com.ahsanislam.kawansogrocery.ui.home

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.ahsanislam.kawansogrocery.MainScreen
import com.ahsanislam.kawansogrocery.R
import com.ahsanislam.kawansogrocery.RoomDB.LocalDB
import com.ahsanislam.kawansogrocery.adapters.GroceryItemAdapter
import com.ahsanislam.kawansogrocery.adapters.GroceryListAdapter
import com.ahsanislam.kawansogrocery.databinding.FragmentAllListBinding
import com.ahsanislam.kawansogrocery.databinding.FragmentHomeBinding
import com.ahsanislam.kawansogrocery.showOptions


class HomeFragment : Fragment() {

    private var viewOfLayout: FragmentAllListBinding? = null
    lateinit var groceryListAdapter: GroceryListAdapter

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = viewOfLayout!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewOfLayout = FragmentAllListBinding.inflate(inflater, container, false)
        val root: View = binding.root
        initRecycler()
        (requireActivity() as MainScreen).binding.btnAdd.setOnClickListener {
            (requireActivity() as MainScreen).navController.navigate(R.id.navigation_create_list)
        }

        viewOfLayout?.title?.setText("Home")
        return root
    }

    fun initRecycler() {
        groceryListAdapter = GroceryListAdapter {view, item, position ,isLongCLick->

            if(!isLongCLick){
                val b = Bundle()
                b.putInt("selectedListId", item.id)
                (requireActivity() as MainScreen).navController.navigate(R.id.navigation_grocery_list_view, b)
            }
            else{
                arrayListOf<String>("Edit","Duplicate").showOptions(requireActivity(),view){pos,str->
                    val b = Bundle()
                    b.putInt("selectedListId", item.id)
                    b.putString("operation", str)
                    (requireActivity() as MainScreen).navController.navigate(R.id.navigation_create_list, b)
                }
            }


        }
        LocalDB.getDatabase(requireContext())?.groceryDAO()?.getAll()?.let {
            it.forEach { grocerList->
                if(grocerList.groceryItems?.filter { x->x.doneStatus }?.size!=0){
                    groceryListAdapter.add(grocerList)
                }
            }

        }

        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        viewOfLayout?.rvGroceryList?.let {
            itemTouchHelper.attachToRecyclerView(it)
            it.adapter = groceryListAdapter
        }

    }

    var simpleItemTouchCallback: ItemTouchHelper.SimpleCallback = object :
        ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT or ItemTouchHelper.DOWN or ItemTouchHelper.UP
        ) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
            //Remove swiped item from list and notify the RecyclerView
            val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
                .setMessage("Are you sure you want to delete this list?")
                .setPositiveButton("Yes, Delete") { dialogInterface, i ->
                    val position = viewHolder.adapterPosition
                    groceryListAdapter.get(position)?.let {
                        LocalDB.getDatabase(requireContext())?.groceryDAO()?.delete(it.id)
                        groceryListAdapter.delete(position)
                    }

                    // Dismiss Dialog
                    dialogInterface.dismiss()
                }
                .setNegativeButton("Cancel") { dialogInterface, i -> dialogInterface.cancel() }
            builder.show()

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewOfLayout = null
    }
}