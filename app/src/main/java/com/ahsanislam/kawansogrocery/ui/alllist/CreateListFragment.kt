package com.ahsanislam.kawansogrocery.ui.alllist

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.ahsanislam.kawansogrocery.MainScreen
import com.ahsanislam.kawansogrocery.RoomDB.LocalDB
import com.ahsanislam.kawansogrocery.adapters.GroceryCreateItemAdapter
import com.ahsanislam.kawansogrocery.databinding.FragmentCreateListBinding
import com.ahsanislam.kawansogrocery.models.GroceryItem
import com.ahsanislam.kawansogrocery.models.GroceryListItem


class CreateListFragment : Fragment() {

    private var viewOfLayout: FragmentCreateListBinding? = null
    lateinit var groceryCreateItemAdapter: GroceryCreateItemAdapter
    var groceryList = GroceryListItem()
    var operationString: String? = "Add"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewOfLayout = FragmentCreateListBinding.inflate(inflater, container, false)
        val root: View = viewOfLayout!!.root

        arguments?.let {
            if (it.containsKey("operation")) {
                val selectedListId = it.getInt("selectedListId")
                operationString = it.getString("operation")
                groceryList =
                    LocalDB.getDatabase(requireContext())?.groceryDAO()?.get(selectedListId)!!
                viewOfLayout?.etName?.setText(groceryList.name)
                if (operationString.equals("Edit")) {
                    viewOfLayout?.title?.text = "Edit List"
                } else {
                    viewOfLayout?.title?.text = "Duplicating ${groceryList.name}"
                    groceryList.id=Math.random().toInt()
                }
            }
        }
        initRecycler()
        viewOfLayout?.btnAddItem?.setOnClickListener {
            val itemName = viewOfLayout?.etItemName?.text.toString()
            val itemAmount = viewOfLayout?.etItemPrice?.text.toString()
            if (itemName.isNotEmpty() && itemAmount.isNotEmpty()) {
                viewOfLayout?.etItemName?.text?.clear()
                viewOfLayout?.etItemPrice?.text?.clear()

                if (itemToEditPosition == -1) {
                    //add case
                    val item = GroceryItem(
                        Math.random().toInt(),
                        itemName,
                        itemAmount.toDouble(),
                        false
                    )
                    groceryCreateItemAdapter.add(item)
                    groceryList.groceryItems?.add(item)
                } else {
                    itemToEdit?.name = itemName
                    itemToEdit?.ammount = itemAmount.toDouble()
                    val item = GroceryItem(
                        itemToEdit!!.id,
                        itemName,
                        itemAmount.toDouble(),
                        itemToEdit!!.doneStatus
                    )
                    groceryCreateItemAdapter.update(item, itemToEditPosition!!)
                    groceryList.groceryItems?.set(itemToEditPosition!!, item)

                    //reset variables
                    itemToEdit = null
                    itemToEditPosition = -1
                }

                viewOfLayout?.etItemName?.requestFocus()//to focus on first field to swiftly add new entry

            } else
                Toast.makeText(
                    requireContext(),
                    "Please fill the details first",
                    Toast.LENGTH_SHORT
                ).show()
        }
        (requireActivity() as MainScreen).binding.btnAdd.setOnClickListener {

            if (groceryCreateItemAdapter.itemCount != 0) {
                val listName = viewOfLayout?.etName?.text.toString()
                when {
                    listName.isEmpty() -> {
                        Toast.makeText(
                            requireContext(),
                            "Please enter list name",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    !operationString.equals("Edit") && LocalDB.getDatabase(requireContext())
                        ?.groceryDAO()
                        ?.get(listName) != null -> {
                        Toast.makeText(
                            requireContext(),
                            "$listName name already present, Consider changing name of the list",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    else -> {
                        groceryList.name = listName
                        LocalDB.getDatabase(requireContext())?.groceryDAO()?.add(groceryList)
                        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
                            .setMessage("Your list is saved!")
                            .setPositiveButton("Ok") { dialogInterface, i ->
                                // Dismiss Dialog
                                dialogInterface.dismiss()
                                (requireActivity() as MainScreen).onBackPressed()
                            }
                        builder.show()
                    }
                }

            } else {
                Toast.makeText(requireContext(), "Please add some items first", Toast.LENGTH_SHORT)
                    .show()
            }


        }

        return root


    }

    var itemToEditPosition: Int? = -1
    var itemToEdit: GroceryItem? = null
    fun initRecycler() {
        groceryCreateItemAdapter = GroceryCreateItemAdapter { item, position ->
            itemToEditPosition = position
            itemToEdit = item
            viewOfLayout?.etItemName?.setText(item.name)
            viewOfLayout?.etItemPrice?.setText(item.ammount.toString())
        }

        if (!groceryList.groceryItems.isNullOrEmpty()) {
            groceryCreateItemAdapter.addAll(groceryList.groceryItems!!)
        }

        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        viewOfLayout?.rvGroceryList?.let {
            itemTouchHelper.attachToRecyclerView(it)
            it.adapter = groceryCreateItemAdapter
        }

    }

    var simpleItemTouchCallback: ItemTouchHelper.SimpleCallback = object :
        ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT or ItemTouchHelper.DOWN or ItemTouchHelper.UP
        ) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
            //Remove swiped item from list and notify the RecyclerView
            val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
                .setMessage("Are you sure you want to delete this list?")
                .setPositiveButton("Yes, Delete") { dialogInterface, i ->
                    val position = viewHolder.adapterPosition
                    groceryCreateItemAdapter.delete(position)
                    // Dismiss Dialog
                    dialogInterface.dismiss()
                }
                .setNegativeButton("Cancel") { dialogInterface, i -> dialogInterface.cancel() }
            builder.show()

        }
    }


}