package com.ahsanislam.kawansogrocery.RoomDB

import androidx.room.*
import com.ahsanislam.kawansogrocery.models.GroceryListItem

@Dao
interface GroceryDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(obj: GroceryListItem)

    @Query("SELECT * FROM GroceryListItem ORDER BY timestamp DESC")
    fun getAll(): List<GroceryListItem>

    @Query("SELECT * FROM GroceryListItem WHERE id =:mId")
    fun get(mId: Int): GroceryListItem

    @Query("SELECT * FROM GroceryListItem WHERE name =:name")
    fun get(name: String): GroceryListItem?

    @Update
    fun update(obj: GroceryListItem)

    @Query("DELETE FROM GroceryListItem WHERE id =:mId")
    fun delete(mId: Int)

    @Query("SELECT COUNT() FROM GroceryListItem ")
    fun count(): Int

}