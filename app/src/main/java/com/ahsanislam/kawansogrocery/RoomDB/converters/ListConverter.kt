package com.ahsanislam.kawansogrocery.RoomDB.converters

import androidx.room.TypeConverter
import com.ahsanislam.kawansogrocery.models.GroceryItem
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

object ListConverter {
    @TypeConverter
    fun fromString(value: String?): ArrayList<GroceryItem> {
        val listType: Type = object : TypeToken<ArrayList<GroceryItem?>?>() {}.getType()
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: ArrayList<GroceryItem?>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}