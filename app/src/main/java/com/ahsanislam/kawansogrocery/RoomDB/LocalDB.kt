package com.ahsanislam.kawansogrocery.RoomDB

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ahsanislam.kawansogrocery.RoomDB.converters.ListConverter
import com.ahsanislam.kawansogrocery.models.GroceryItem
import com.ahsanislam.kawansogrocery.models.GroceryListItem

@Database(entities = [GroceryListItem::class], version = 1)
@TypeConverters( ListConverter::class)
abstract class LocalDB : RoomDatabase() {
    abstract fun groceryDAO(): GroceryDAO?

    companion object {
        private var myAppDatabase: LocalDB? = null
        fun getDatabase(context: Context?): LocalDB? {
            if (myAppDatabase == null) {
                myAppDatabase =
                    Room.databaseBuilder<LocalDB>(context!!, LocalDB::class.java, "kawanso_grocery")
                        .allowMainThreadQueries().build()
            }
            return myAppDatabase
        }
    }
}