package com.ahsanislam.kawansogrocery.adapters

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ahsanislam.kawansogrocery.databinding.SingleGroceryListViewBinding
import com.ahsanislam.kawansogrocery.models.GroceryListItem


class GroceryListAdapter(
    val groceryList: ArrayList<GroceryListItem>? = ArrayList(),
    val Callback: (View, GroceryListItem, Int, Boolean) -> Unit?
) :
    RecyclerView.Adapter<GroceryListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewBinding: SingleGroceryListViewBinding =
            SingleGroceryListViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return ViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = groceryList?.get(position)

        holder.binding.txtTitle.text = item!!.name

        val count=item.groceryItems?.filter { x->x.doneStatus }?.size
        if(count==item.groceryItems?.size){
            holder.binding.txtTitle.paintFlags =
                holder.binding.txtTitle.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        }
        holder.binding.root.setOnClickListener {
            Callback.invoke(holder.binding.root,groceryList?.get(position)!!, position,false)
        }

        holder.binding.root.setOnLongClickListener {
            Callback.invoke(holder.binding.root,groceryList?.get(position)!!, position,true)
            true
        }
    }

    override fun getItemCount(): Int {
        return groceryList!!.size
    }

    fun getData(): ArrayList<GroceryListItem>? {
        return groceryList
    }

    fun add(item: GroceryListItem) {
        groceryList?.add(item)
        groceryList?.size?.minus(1)?.let { notifyItemInserted(it) }
    }

    fun addAll(item: List<GroceryListItem>) {
        groceryList?.addAll(item)
        notifyDataSetChanged()
    }

    fun update(item: GroceryListItem, position: Int) {
        groceryList?.set(position, item)
        notifyItemChanged(position)
    }

    fun get(position: Int): GroceryListItem? {
        return groceryList?.get(position)
    }

    fun delete(position: Int) {
        groceryList?.removeAt(position)
        notifyItemRemoved(position)
    }


    class ViewHolder(val binding: SingleGroceryListViewBinding) :
        RecyclerView.ViewHolder(binding.root)
}