package com.ahsanislam.kawansogrocery.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ahsanislam.kawansogrocery.databinding.SingleGroceryCreateItemBinding
import com.ahsanislam.kawansogrocery.databinding.SingleGroceryViewBinding
import com.ahsanislam.kawansogrocery.models.GroceryItem


class GroceryCreateItemAdapter(
    val groceryList: ArrayList<GroceryItem>? = ArrayList(),
    val Callback: (GroceryItem, Int) -> Unit?
) :
    RecyclerView.Adapter<GroceryCreateItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewBinding: SingleGroceryCreateItemBinding =
            SingleGroceryCreateItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return ViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = groceryList?.get(position)
        holder.binding.txtTitle.text = item!!.name

        holder.binding.root.setOnClickListener {
            Callback.invoke(item, position)
        }
    }

    override fun getItemCount(): Int {
        return groceryList!!.size
    }

    fun getData(): ArrayList<GroceryItem>? {
        return groceryList
    }

    fun add(item: GroceryItem) {
        groceryList?.add(item)
        groceryList?.size?.minus(1)?.let { notifyItemInserted(it) }
    }

    fun addAll(item: List<GroceryItem>) {
        groceryList?.addAll(item)
        notifyDataSetChanged()
    }

    fun update(item: GroceryItem, position: Int) {
        groceryList?.set(position, item)
        notifyItemChanged(position)
    }

    fun get(position: Int): GroceryItem? {
        return groceryList?.get(position)
    }

    fun delete(position: Int) {
        groceryList?.removeAt(position)
        notifyItemRemoved(position)
    }


    class ViewHolder(val binding: SingleGroceryCreateItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}